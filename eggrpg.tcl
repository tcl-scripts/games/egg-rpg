########################################
#              Eggdrop RPG             #
#                                      #
# A short RPG game on IRC              #
########################################

namespace eval ::eggrpg {
	
	
	variable lang "fr"
	
	package require msgcat
	package require sqlite3
	
	### DO NOT EDIT BELOW
	variable author "CrazyCat <https://forum.eggdrop.fr>"
	variable version "0.1.0"
	
	variable basepath [file dirname [file normalize [info script]]]
	variable rpgdb "[set [namespace current]::basepath]/eggrpg/game.db"
	variable cnx

	proc db.open {} {
		sqlite3 [namespace current]::cnx [set [namespace current]::rpgdb]
	}

	proc db.close {} {
		[namespace current]::cnx close
	}
	
	proc init {} {
		# Script initialization:
	# Creating databases if needed, filling them
	# loading langages
	proc init {} {
		[namespace current]::i18n "[set [namespace current]::basepath]/eggrpg/languages/lang-$::eggrpg::lang.msg" $::eggrpg::lang
		[namespace current]::db.open
		[namespace current]::db eval {CREATE TABLE IF NOT EXISTS player (pid integer primary key autoincrement, nick text, level integer, xp integer, hp integer, att integer, def integer, gold integer)}
		[namespace current]::db eval {CREATE INDEX IF NOT EXISTS pid ON player(pid)}
		[namespace current]::db eval {CREATE INDEX IF NOT EXISTS pnick ON player(nick)}
		[namespace current]::db eval {CREATE TABLE IF NOT EXISTS rpi (pid integer, iname text, equiped integer)}
		[namespace current]::db eval {CREATE TABLE IF NOT EXISTS items (iname text primary key, idesc text, tname text, levmin integer, hpmod integer, attmod integer, defmod integer, cost integer)}
		[namespace current]::db eval {CREATE INDEX IF NOT EXISTS iname ON items(iname)}
		[namespace current]::db eval {CREATE TABLE IF NOT EXISTS itemtype (tname text primary key, maxowned integer)}
		[namespace current]::db eval {CREATE INDEX IF NOT EXISTS titems ON itemtype(tname)}
		[namespace current]::db eval {CREATE TABLE IF NOT EXISTS nicks (nick text primary key, altnick text)}
		[namespace current]::db eval {CREATE INDEX IF NOT EXISTS nick ON nicks(nick)}
		
		set fi [::ini::open [set [namespace current]::basepath]/eggrpg/initrpg-$::eggrpg::lang.ini]
		array set itypes [::ini::get $fi "itemstype"]
		foreach type [array names itypes] {
			set mo [expr $itypes($type)]
			[namespace current]::cnx eval {INSERT INTO itemtype (tname, maxowned) SELECT $type, $mo WHERE NOT EXISTS (SELECT 1 FROM itemtype WHERE tname=$type)}
			array set items [::ini::get $fi $type]
			lappend [namespace current]::itemtype $type
			if {$::eggrpg::lang!="" && $::eggrpg::lang!="en"} {
				lappend ::eggrpg::itemtypei18n "$type ([::msgcat::mc $type])"
			} else {
				lappend ::eggrpg::itemtypei18n $type
			}
			foreach item [array names items] {
				set caracs [split $items($item) |]
				set idesc [lindex $caracs 0]
				set levmin [expr [lindex $caracs 1]]
				set hpmod [expr [lindex $caracs 2]]
				set attmod [expr [lindex $caracs 3]]
				set defmod [expr [lindex $caracs 4]]
				set cost [expr [lindex $caracs 5]]
				[namespace current]::cnx eval {INSERT INTO items (iname, idesc, tname, levmin, hpmod, attmod, defmod, cost) SELECT $item, $idesc, $type, $levmin, $hpmod, $attmod, $defmod, $cost WHERE NOT EXISTS (SELECT 1 FROM items WHERE iname=$item)}
			}
		}
		[namespace current]::db.close
		putlog "EggRPG v[set [namespace current]::version] by [set [namespace current]::author] loaded"
	}
	
	# retrieve a player
	# returns the player id or 0
	proc playerfind { nick } {
		set dbnick [string tolower $nick]
		[namespace current]::db.open
		set exists [[namespace current]::cnx eval {SELECT pid FROM player p left join nicks n on n.nick=p.nick WHERE (p.nick=$dbnick or n.altnick=$dbnick)}]
		[namespace current]::db.close
		# putlog $exists
		if {[llength $exists]>0} { return [lindex $exists 0] } else { return 0}
	}
	
	bind msg - "!play" ::eggrpg::playercreate
	# Creates a player with default values
	proc playercreate {nick uhost handle text} {
		set player [::eggrpg::playerfind $nick]
		if {$player!=0} {
			putserv "PRIVMSG $nick :[::msgcat::mc "Well, you're already playing"]"
			return 1
		}
		set dbnick [string tolower $nick]
		[namespace current]::db.open
		[namespace current]::cnx eval {INSERT INTO player (nick, level, xp, hp, att, def, gold) VALUES ($dbnick, 1, 0, 10, 10, 10, 100)}
		[namespace current]::cnx eval {INSERT INTO nicks (nick, altnick) VALUES ($nick, $dbnick)}
		[namespace current]::db.close
		putserv "PRIVMSG $nick :[format [msgcat::mc "You are now registered as %s"] $nick]"
	}
	
	bind msg - "!stats" ::eggrpg::playerstats
	
	# Loads all stats about a player
	proc playerstats {nick uhost handle text} {
		if {[::eggrpg::playerfind $nick]==0} {
			putserv "PRIVMSG $nick :[::msgcat::mc "Sorry but it seems you aren't playing"]"
			return 1
		}
	}
	
	# Loads a full player infos
	proc playerload {nick} {
		set player [::eggrpg::playerfind $nick]
		if { $player == 0} {
			putserv "PRIVMSG $nick :[format [::msgcat::mc "%s is unknown"] $nick]"
			return 0
		}
		[namespace current]::db.open
		set curdbplayer [::eggrpg::db eval {SELECT p.level, p.xp, p.gold FROM player p WHERE p.pid=$player}]
		if {llength $curdbplayer]<=0} {
			putserv "PRIVMSG $nick :[format [::msgcat::mc "%s is unknown"] $nick]"
			return 0
		}
		set curplayer(level) [lindex $curdbplayer 0]
		set curplayer(xp) [lindex $curdbplayer 1]
		set curplayer(gold) [lindex $curdbplayer 2]
		[namespace current]::cnx eval {SELECT t.tname, i.iname, i.hpmod, i.attmod, i.defmod, it.maxowned FROM rpi LEFT JOIN items i ON i.iname=rpi.iname JOIN itemtype it on it.tname=i.tname WHERE p.pid=$player} items {
			putlog "Items de $nick"
			foreach nom [array names items] {
				putlog "$items($nom)"
			}
		}
	}
	
	
	bind msg - "!shop" ::eggrpg::shop
	# Main shopping procedure
	proc shop {nick uhost handle text} {
		set player [::eggrpg::playerload $nick]
		if {[::eggrpg::playerfind $nick]==0} {
			putserv "PRIVMSG $nick :[::msgcat::mc "Sorry but it seems you aren't playing"]"
			return 1
		}
		if {[llength [split $text]]==0} {
			putserv "PRIVMSG $nick :[::msgcat::mc "You must choose an item to shop: !shop <item>"]"
			putserv "PRIVMSG $nick :[::msgcat::mc "You can get list of items with the command !shop list"]"
			putserv "PRIVMSG $nick :[::msgcat::mc "or filter by category using !shop <category>"]"
			set ilist [join $::eggrpg::itemtypei18n ", "]
			putserv "PRIVMSG $nick :[::msgcat::mc "Available categories:"] $ilist"
			return 1
		}
		set cat [string tolower [lindex [split $text] 0]]
		if { [lsearch $::eggrpg::itemtype $cat] > -1 } {
			# Item is a category
			[namespace current]::db.open
			set count [[namespace current]::cnx eval {SELECT * from items WHERE tname=$cat}]
			[namespace current]::db.close
			if {[llength $count]>0} {
				for {set i 0} {$i < [llength $count]} { incr i 8} {
					set name [lindex $count $i]
					set desc [lindex $count $i+1]
					set levmin [lindex $count $i+3]
					set hpmod [lindex $count $i+4]
					set attmod [lindex $count $i+5]
					set defmod [lindex $count $i+6]
					set cost [lindex $count $i+7]
					putserv "PRIVMSG $nick :\00308$name\003 : $desc ($cost po)  - \00312HP: $hpmod\003 / \00304ATT: $attmod\003 / \00303DEF: $defmod\003"
				}
			} else {
				putserv "PRIVMSG $nick :[format [::msgcat::mc "Oups ! I didn't find any item in %s"] $cat]"
			}
			return 1
		}
		[namespace current]::db.open
		set count [::eggrpg::db eval {SELECT i.iname, i.idesc, i.tname, i.levmin, i.hpmod, i.attmod, i.defmod, i.cost, it.maxowned from items i join itemtype it on it.tname=i.tname WHERE iname=$cat}]
		if {[llength $count]>0} {
			set name [lindex $count 0]
			set desc [lindex $count 1]
			set categ [lindex $count 2]
			set levmin [lindex $count 3]
			set hpmod [lindex $count 4]
			set attmod [lindex $count 5]
			set defmod [lindex $count 6]
			set cost [lindex $count 7]
			[namespace current]::db.close
		} else {
			[namespace current]::db.close
			putserv "PRIVMSG $nick :[format [::msgcat::mc "Cannot find item called %s"] $cat]"
			
		}
		
	}
	
	[namespace current]::init
}
