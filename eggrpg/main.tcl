########################################
#              Eggdrop RPG             #
#                                      #
# A short RPG game on IRC              #
########################################

## DO NOT EDIT ANYTHING BELOW
# To edit configuration, open eggrpg.tcl
#

namespace eval ::eggrpg {
	
	package require sqlite3
	package require inifile
	package require msgcat
	
	variable itemtype
	variable itemtypei18n
		
	proc opendb {} {
		sqlite3 ::eggrpg::db $::eggrpg::rpgdb
	}

	proc closedb {} {
		::eggrpg::db close
	}

	# Script initialization:
	# Creating databases if needed, filling them
	# loading langages
	proc init {} {
		::eggrpg::i18n "scripts/eggrpg/languages/lang-$::eggrpg::lang.msg" $::eggrpg::lang
		::eggrpg::opendb
		::eggrpg::db eval {CREATE TABLE IF NOT EXISTS player (pid integer primary key autoincrement, nick text, level integer, xp integer, hp integer, att integer, def integer, gold integer)}
		::eggrpg::db eval {CREATE INDEX IF NOT EXISTS pid ON player(pid)}
		::eggrpg::db eval {CREATE INDEX IF NOT EXISTS pnick ON player(nick)}
		::eggrpg::db eval {CREATE TABLE IF NOT EXISTS rpi (pid integer, iname text, equiped integer)}
		::eggrpg::db eval {CREATE TABLE IF NOT EXISTS items (iname text primary key, idesc text, tname text, levmin integer, hpmod integer, attmod integer, defmod integer, cost integer)}
		::eggrpg::db eval {CREATE INDEX IF NOT EXISTS iname ON items(iname)}
		::eggrpg::db eval {CREATE TABLE IF NOT EXISTS itemtype (tname text primary key, maxowned integer)}
		::eggrpg::db eval {CREATE INDEX IF NOT EXISTS titems ON itemtype(tname)}
		::eggrpg::db eval {CREATE TABLE IF NOT EXISTS nicks (nick text primary key, altnick text)}
		::eggrpg::db eval {CREATE INDEX IF NOT EXISTS nick ON nicks(nick)}
		
		set fi [::ini::open scripts/eggrpg/initrpg-$::eggrpg::lang.ini]
		array set itypes [::ini::get $fi "itemstype"]
		foreach type [array names itypes] {
			set mo [expr $itypes($type)]
			::eggrpg::db eval {INSERT INTO itemtype (tname, maxowned) SELECT $type, $mo WHERE NOT EXISTS (SELECT 1 FROM itemtype WHERE tname=$type)}
			array set items [::ini::get $fi $type]
			lappend ::eggrpg::itemtype $type
			if {$::eggrpg::lang!="" && $::eggrpg::lang!="en"} {
				lappend ::eggrpg::itemtypei18n "$type ([::msgcat::mc $type])"
			} else {
				lappend ::eggrpg::itemtypei18n $type
			}
			foreach item [array names items] {
				set caracs [split $items($item) |]
				set idesc [lindex $caracs 0]
				set levmin [expr [lindex $caracs 1]]
				set hpmod [expr [lindex $caracs 2]]
				set attmod [expr [lindex $caracs 3]]
				set defmod [expr [lindex $caracs 4]]
				set cost [expr [lindex $caracs 5]]
				::eggrpg::db eval {INSERT INTO items (iname, idesc, tname, levmin, hpmod, attmod, defmod, cost) SELECT $item, $idesc, $type, $levmin, $hpmod, $attmod, $defmod, $cost WHERE NOT EXISTS (SELECT 1 FROM items WHERE iname=$item)}
			}
		}
		::eggrpg::closedb
	}

	# retrieve a player
	# returns the player id or 0
	proc playerfind { nick } {
		set dbnick [string tolower $nick]
		::eggrpg::opendb
		set exists [::eggrpg::db eval {SELECT pid FROM player p left join nicks n on n.nick=p.nick WHERE (p.nick=$dbnick or n.altnick=$dbnick)}]
		::eggrpg::closedb
		# putlog $exists
		if {[llength $exists]>0} { return [lindex $exists 0] } else { return 0}
	}
	
	bind msg - "!play" ::eggrpg::playercreate
	# Creates a player with default values
	proc playercreate {nick uhost handle text} {
		set player [::eggrpg::playerfind $nick]
		if {$player!=0} {
			putserv "PRIVMSG $nick :[::msgcat::mc "Well, you're already playing"]"
			return 1
		}
		set dbnick [string tolower $nick]
		::eggrpg::opendb
		::eggrpg::db eval {INSERT INTO player (nick, level, xp, hp, att, def, gold) VALUES ($dbnick, 1, 0, 10, 10, 10, 100)}
		::eggrpg::db eval {INSERT INTO nicks (nick, altnick) VALUES ($nick, $dbnick)}
		::eggrpg::closedb
		putserv "PRIVMSG $nick :[format [msgcat::mc "You are now registered as %s"] $nick]"
	}
	
	bind msg - "!stats" ::eggrpg::playerstats
	
	# Loads all stats about a player
	proc playerstats {nick uhost handle text} {
		if {[::eggrpg::playerfind $nick]==0} {
			putserv "PRIVMSG $nick :[::msgcat::mc "Sorry but it seems you aren't playing"]"
			return 1
		}
	}
	
	# Loads a full player infos
	proc playerload {nick} {
		set player [::eggrpg::playerfind $nick]
		if { $player == 0} {
			putserv "PRIVMSG $nick :[format [::msgcat::mc "%s is unknown"] $nick]"
			return 0
		}
		::eggrpg::opendb
		set curdbplayer [::eggrpg::db eval {SELECT p.level, p.xp, p.gold FROM player p WHERE p.pid=$player}]
		if {llength $curdbplayer]<=0} {
			putserv "PRIVMSG $nick :[format [::msgcat::mc "%s is unknown"] $nick]"
			return 0
		}
		set curplayer(level) [lindex $curdbplayer 0]
		set curplayer(xp) [lindex $curdbplayer 1]
		set curplayer(gold) [lindex $curdbplayer 2]
		::eggrpg::db eval {SELECT t.tname, i.iname, i.hpmod, i.attmod, i.defmod, it.maxowned FROM rpi LEFT JOIN items i ON i.iname=rpi.iname JOIN itemtype it on it.tname=i.tname WHERE p.pid=$player} items {
			putlog "Items de $nick"
			foreach nom [array names items] {
				putlog "$items($nom)"
			}
		}
	}
	
	
	bind msg - "!shop" ::eggrpg::shop
	# Main shopping procedure
	proc shop {nick uhost handle text} {
		set player [::eggrpg::playerload $nick]
		if {[::eggrpg::playerfind $nick]==0} {
			putserv "PRIVMSG $nick :[::msgcat::mc "Sorry but it seems you aren't playing"]"
			return 1
		}
		if {[llength [split $text]]==0} {
			putserv "PRIVMSG $nick :[::msgcat::mc "You must choose an item to shop: !shop <item>"]"
			putserv "PRIVMSG $nick :[::msgcat::mc "You can get list of items with the command !shop list"]"
			putserv "PRIVMSG $nick :[::msgcat::mc "or filter by category using !shop <category>"]"
			set ilist [join $::eggrpg::itemtypei18n ", "]
			putserv "PRIVMSG $nick :[::msgcat::mc "Available categories:"] $ilist"
			return 1
		}
		set cat [string tolower [lindex [split $text] 0]]
		if { [lsearch $::eggrpg::itemtype $cat] > -1 } {
			# Item is a category
			::eggrpg::opendb
			set count [::eggrpg::db eval {SELECT * from items WHERE tname=$cat}]
			::eggrpg::closedb
			if {[llength $count]>0} {
				for {set i 0} {$i < [llength $count]} { incr i 8} {
					set name [lindex $count $i]
					set desc [lindex $count $i+1]
					set levmin [lindex $count $i+3]
					set hpmod [lindex $count $i+4]
					set attmod [lindex $count $i+5]
					set defmod [lindex $count $i+6]
					set cost [lindex $count $i+7]
					putserv "PRIVMSG $nick :\00308$name\003 : $desc ($cost po)  - \00312HP: $hpmod\003 / \00304ATT: $attmod\003 / \00303DEF: $defmod\003"
				}
			} else {
				putserv "PRIVMSG $nick :[format [::msgcat::mc "Oups ! I didn't find any item in %s"] $cat]"
			}
			return 1
		}
		::eggrpg::opendb
		set count [::eggrpg::db eval {SELECT i.iname, i.idesc, i.tname, i.levmin, i.hpmod, i.attmod, i.defmod, i.cost, it.maxowned from items i join itemtype it on it.tname=i.tname WHERE iname=$cat}]
		if {[llength $count]>0} {
			set name [lindex $count 0]
			set desc [lindex $count 1]
			set categ [lindex $count 2]
			set levmin [lindex $count 3]
			set hpmod [lindex $count 4]
			set attmod [lindex $count 5]
			set defmod [lindex $count 6]
			set cost [lindex $count 7]
			::eggrpg::closedb
		} else {
			::eggrpg::closedb
			putserv "PRIVMSG $nick :[format [::msgcat::mc "Cannot find item called %s"] $cat]"
			
		}
		
	}
	
	# internationnalization
	proc i18n {msgfile {lang {}} } {
		if {$lang == "" || $lang == "en"} {set lang [string range [::msgcat::mclocale] 0 1] }
		if { [catch {open $msgfile r} fmsg] } {
			putlog "Could not open $msgfile for reading\n$fmsg"
		} else {
			putlog "Loading $lang file from $msgfile"
			while {[gets $fmsg ligne] >= 0} {
				lappend ll $ligne
			}
			close $fmsg
			::msgcat::mcmset $lang [join $ll]
			unset ll
		}
	}
	
	
}